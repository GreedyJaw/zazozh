<?php echo $header; ?>

<main id="content">
  <section class="main-title">
    <div class="container">
      <header>
        <h1>Личный кабинет</h1>
      </header>
      <div class="breadcrumb">
        <ul>
          <li><a href="https://zazozh.cf83387.tmweb.ru/"><span>Главная</span></a> </li>
          <li><span>Личный кабинет</span></li>
        </ul>
      </div>
    </div>
  </section>
</main>

<div class="container">
  <div class="row lk-row flex-lg-row flex-column-reverse">
    <?php if ($column_right) { ?>
    <?php $class = 'col-lg-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12'; ?>
    <?php } ?>
    <div id="content" class="mt-lg-0 mt-4 <?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <fieldset class="inputs-group">

            <label class="control-label"><?php echo $entry_newsletter; ?></label>

            <?php if ($newsletter) { ?>
              <div class="input radio">
                <input type="radio" name="newsletter" value="1" checked="checked" id="newsletter-yes" />
                <label for="newsletter-yes"><?php echo $text_yes; ?> </label>
              </div>

              <div class="input radio">
                <input type="radio" name="newsletter" value="0" id="newsletter-no" />
                <label for="newsletter-no"><?php echo $text_no; ?></label>
              </div>
            <?php } else { ?>
              <div class="input radio">
                <input type="radio" name="newsletter" value="1" id="newsletter-yes" />
                <label for="newsletter-yes"><?php echo $text_yes; ?> </label>
              </div>

              <div class="input radio">
                <input type="radio" name="newsletter" value="0" id="newsletter-no" checked="checked" />
                <label for="newsletter-no"><?php echo $text_no; ?></label>
              </div>
            <?php } ?>

        </fieldset>
        <div class="buttons">
          <input type="submit" value="Сохранить" class="btn green" />
        </div>
      </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>