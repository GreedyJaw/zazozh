<h2>Забыли пароль</h2>
<p><?php echo $text_email; ?></p>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" id="forgottenForm">
    <fieldset class="inputs-group mb-0">
        <div class="input required">
            <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
            <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
        </div>
    </fieldset>
    <div class="buttons clearfix mt-3">
        <input type="submit" value="<?php echo $button_continue; ?>" class="btn green ml-0" name="forgotten" />
    </div>
</form>

<script>
    $('#forgottenForm').submit(function(e){
       e.preventDefault();

       $('#forgottenForm').prev('.alert').remove();

       $.post('index.php?route=account/register/forgotten', $('#forgottenForm').serialize(), function(json) {
            if(!json.success) {
                $('#forgottenForm').before('<div class="alert alert-danger">' + json.error + '</div>');
            } else {
                $('#forgottenForm').trigger('reset');
                $('#forgottenModal').modal('hide');
                $('#successModal .modal-content p').text(json.success);
                $('#successModal').modal('show');
            }
       }, 'json').fail(function(err) {
            console.log(err);
       });
    });
</script>