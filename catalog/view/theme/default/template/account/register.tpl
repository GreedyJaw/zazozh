<?php echo $header; ?>


<main id="content">
			<section class="main-title">
				<div class="container">
					<header>
						<h1>Вход/регистрация</h1>
					</header>
					<div class="breadcrumb">
						<ul>
							<li><a href="/"><span>Главная</span></a></li>
							<li>Вход/регистрация</li>
						</ul>
					</div>
				</div>
			</section>
		</main>
		<div class="container">
			<div class="row flex-column-reverse flex-xl-row">
				<?php echo $column_left; ?>
				<main id="sign-in" class="col-xl-9">
					<section class="single">
						<div class="row">



							<div class="col-lg-6 login inputs-group">
								<div>

            						<?php echo $login_form; ?>

									
								</div>
							</div>




							<div class="col-lg-6 register inputs-group">
								<div>

									<?php echo $register_form; ?>

								</div>
							</div>
						</div>
					</section>
				</main>
			</div>
		</div>

<?php echo $footer; ?>