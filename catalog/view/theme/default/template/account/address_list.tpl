<?php echo $header; ?>

<main id="content">
  <section class="main-title">
    <div class="container">
      <header>
        <h1>Личный кабинет</h1>
      </header>
      <div class="breadcrumb">
        <ul>
          <li><a href="https://zazozh.cf83387.tmweb.ru/"><span>Главная</span></a> </li>
          <li><span>Личный кабинет</span></li>
        </ul>
      </div>
    </div>
  </section>
</main>

<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row lk-row flex-lg-row flex-column-reverse">
    <?php if ($column_right) { ?>
    <?php $class = 'col-lg-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12'; ?>
    <?php } ?>
    <div id="content" class="mt-lg-0 mt-4 <?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $text_address_book; ?></h2>
      <?php if ($addresses) { ?>
      <div class="table-responsive" id="address-list">
        <table class="table table-bordered table-hover">
          <?php foreach ($addresses as $result) { ?>
          <tr>
            <td class="text-left"><?php echo $result['address']; ?></td>
            <td class="text-right"><a href="<?php echo $result['update']; ?>" class="btn btn-info"><?php echo $button_edit; ?></a> &nbsp; <a href="<?php echo $result['delete']; ?>" class="btn btn-danger"><?php echo $button_delete; ?></a></td>
          </tr>
          <?php } ?>
        </table>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-left"><a href="<?php echo $back; ?>" class="btn btn-default"><?php echo $button_back; ?></a></div>
        <div class="pull-right"><a href="<?php echo $add; ?>" class="btn green"><?php echo $button_new_address; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>
  if($(window).width() < 768) {
    var html = '<div class="address-list">';

    $('#address-list table tr').each(function() {
        html += '<div class="address-item"><div class="address-text">' + $(this).find('td:first-child').html() + '</div><div class="address-buttons">' + $(this).find('td:last-child').html() + '</div></div>';
    });

    html += '</div>';

    $('#address-list').html(html);
  }
</script>

<?php echo $footer; ?>