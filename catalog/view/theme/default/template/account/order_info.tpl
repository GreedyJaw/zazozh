<?php echo $header; ?>

<main id="content">
  <section class="main-title">
    <div class="container">
      <header>
        <h1>Личный кабинет</h1>
      </header>
      <div class="breadcrumb">
        <ul>
          <li><a href="https://zazozh.cf83387.tmweb.ru/"><span>Главная</span></a> </li>
          <li><span>Личный кабинет</span></li>
        </ul>
      </div>
    </div>
  </section>
</main>

<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row lk-row flex-lg-row flex-column-reverse">
    <?php if ($column_right) { ?>
    <?php $class = 'col-lg-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12'; ?>
    <?php } ?>
    <div id="content" class="mt-lg-0 mt-4 <?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <div id="order-details">
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left" colspan="2"><?php echo $text_order_detail; ?></td>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td class="text-left" style="width: 50%;"><?php if ($invoice_no) { ?>
              <b><?php echo $text_invoice_no; ?></b> <?php echo $invoice_no; ?><br /><br>
              <?php } ?>
              <b><?php echo $text_order_id; ?></b> #<?php echo $order_id; ?><br /><br>
              <b><?php echo $text_date_added; ?></b> <?php echo $date_added; ?><br><br></td>
            <td class="text-left" style="width: 50%;"><?php if ($payment_method) { ?>
              <b><?php echo $text_payment_method; ?></b> <?php echo $payment_method; ?><br /><br>
              <?php } ?>
              <?php if ($shipping_method) { ?>
              <b><?php echo $text_shipping_method; ?></b> <?php echo $shipping_method; ?>
              <?php } ?><br><br>
              <b>Адрес доставки</b>:<br><?php echo $shipping_address; ?>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
      <div class="table-responsive">
        <div id="order-products">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left"><?php echo $column_name; ?></td>
                <td class="text-left"><?php echo $column_model; ?></td>
                <td class="text-right"><?php echo $column_quantity; ?></td>
                <td class="text-right"><?php echo $column_price; ?></td>
                <td class="text-right"><?php echo $column_total; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="text-left"><?php echo $product['name']; ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                  <?php } ?></td>
                <td class="text-left"><?php echo $product['model']; ?></td>
                <td class="text-right"><?php echo $product['quantity']; ?></td>
                <td class="text-right"><?php echo $product['price']; ?></td>
                <td class="text-right"><?php echo $product['total']; ?></td>
              </tr>
              <?php } ?>
              <?php foreach ($vouchers as $voucher) { ?>
              <tr>
                <td class="text-left"><?php echo $voucher['description']; ?></td>
                <td class="text-left"></td>
                <td class="text-right">1</td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
                <td class="text-right"><?php echo $voucher['amount']; ?></td>
              </tr>
              <?php } ?>
            </tbody>
            <tfoot>
              <?php foreach ($totals as $total) { ?>
              <tr>
                <td colspan="3"></td>
                <td class="text-right"><b><?php echo $total['title']; ?></b></td>
                <td class="text-right"><?php echo $total['text']; ?></td>
              </tr>
              <?php } ?>
            </tfoot>
          </table>
        </div>
      </div>
      <?php if ($comment) { ?>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left"><?php echo $text_comment; ?></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $comment; ?></td>
          </tr>
        </tbody>
      </table>
      <?php } ?>
      <?php if ($histories) { ?>
      <h3><?php echo $text_history; ?></h3>
      <div id="order-history">
        <table class="table table-bordered table-hover">
          <thead>
          <tr>
            <td class="text-left"><?php echo $column_date_added; ?></td>
            <td class="text-left"><?php echo $column_status; ?></td>
          </tr>
          </thead>
          <tbody>
          <?php if ($histories) { ?>
          <?php foreach ($histories as $history) { ?>
          <tr>
            <td class="text-left"><?php echo $history['date_added']; ?></td>
            <td class="text-left"><?php echo $history['status']; ?></td>
          </tr>
          <?php } ?>
          <?php } else { ?>
          <tr>
            <td colspan="2" class="text-center"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
          </tbody>
        </table>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>
  if($(window).width() < 768) {
    var html = '<div class="order-details">';

    html += '<div class="order-details-header">' + $('#order-details thead td').text() + '</div>';

    html += '<div class="order-details-body">';

    $('#order-details tbody td').each(function(){
        html += $(this).html();
    });

    html += '</div>';

    html += '</div>';

    $('#order-details').html(html);

    html = '<div class="order-details"><div class="order-details-header">Товары</div>';

    html += '<div class="order-details-body">';

    $('#order-products table tbody tr').each(function(){
        html += '<div class="order-details-product">';

        $(this).find('td').each(function(){
            html += '<div class="order-details-item"><label>' + $('#order-products thead td').eq($(this).index()).text() + '</label><span>' + $(this).html() + '</span></div>';
        });

        html += '</div>';
    });

    html += '<div class="order-details-totals">';

    $('#order-products table tfoot tr').each(function() {
        html += '<div class="order-details-item"><label>' + $(this).find('td').eq(1).html() + '</label><span>' + $(this).find('td').eq(2).html() + '</span></div>';
    });

    html += '</div>';

    html += '</div>';

    html += '</div>';

    $('#order-products').html(html);

    html = '<div class="order-details"><div class="order-details-header">' + $('#order-history').prev().text() + '</div>';

    html += '<div class="order-details-body">';

    $('#order-history table tbody tr').each(function(){
      $(this).find('td').each(function(){
        html += '<div class="order-details-item"><label>' + $('#order-history thead td').eq($(this).index()).text() + '</label><span>' + $(this).html() + '</span></div>';
      });
    });

    html += '</div>';

    html += '</div>';

    $('#order-history').html(html);
    $('#order-history').prev().remove();
  }
</script>

<?php echo $footer; ?>