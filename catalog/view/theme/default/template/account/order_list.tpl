<?php echo $header; ?>

<main id="content">
  <section class="main-title">
    <div class="container">
      <header>
        <h1>Личный кабинет</h1>
      </header>
      <div class="breadcrumb">
        <ul>
          <li><a href="https://zazozh.cf83387.tmweb.ru/"><span>Главная</span></a> </li>
          <li><span>Личный кабинет</span></li>
        </ul>
      </div>
    </div>
  </section>
</main>

<div class="container">
  <div class="row lk-row flex-lg-row flex-column-reverse">
    <?php if ($column_right) { ?>
    <?php $class = 'col-lg-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12'; ?>
    <?php } ?>
    <div id="content" class="mt-lg-0 mt-4 <?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <div id="orders-list">
          <table class="table table-bordered table-hover">
            <thead>
            <tr>
              <td class="text-right"><?php echo $column_order_id; ?></td>
              <td class="text-left"><?php echo $column_customer; ?></td>
              <td class="text-right"><?php echo $column_product; ?></td>
              <td class="text-left"><?php echo $column_status; ?></td>
              <td class="text-right"><?php echo $column_total; ?></td>
              <td class="text-left"><?php echo $column_date_added; ?></td>
              <td></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order) { ?>
            <tr>
              <td class="text-right">#<?php echo $order['order_id']; ?></td>
              <td class="text-left"><?php echo $order['name']; ?></td>
              <td class="text-right"><?php echo $order['products']; ?></td>
              <td class="text-left"><?php echo $order['status']; ?></td>
              <td class="text-right"><?php echo $order['total']; ?></td>
              <td class="text-left"><?php echo $order['date_added']; ?></td>
              <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i> Просмотр</a></td>
            </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-lg-6 text-lg-right"><?php echo $results; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<script>
  if($(window).width() < 768) {
    var html = '<div class="order-list">';

    $('#orders-list table tbody tr').each(function(){
      html += '<div class="order-item">';

      $(this).find('td:not(:last-child)').each(function(){
        html += '<div class="row"><div class="col-6"><b>' + $('#orders-list thead td').eq($(this).index()).text() + '</b></div><div class="col-6">' + $(this).html() + '</div></div>';
      });

      html += '<div class="buttons">' + $(this).find('td:last-child').html() + '</div>';

      html += '</div>';
    });

    html += '</div>';

    $('#orders-list').html(html);
  }
</script>

<?php echo $footer; ?>
