<div class="sort mt-3">
    <div class="container pt-0 pb-0">
        <span>Сортировать по:</span>
        <ul id="sort">
            <li class="active"><a href="#by_name">Наименованию</a></li>
            <li><a href="#by_new">Новинкам</a></li>
            <li><a href="#by_price_up">Дешевле</a></li>
            <li><a href="#by_price_down">Дороже</a></li>
            <li><a href="#by_hit">Хиты продаж</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="row">
        <main id="products" class="col-xl-9">
            <?php foreach($categories as $category) { ?>
            <section id="category<?php echo $category['category_id']; ?>" class="list" data-sorts="<?php echo htmlspecialchars(json_encode($category['sorts'])); ?>">

                <header>

                    <h2><?php echo $category['name']; ?></h2>

                </header>

                <div class="row list-items">

                    <?php foreach ($category['products'] as $product) { ?>

                    <div class="col-6 col-lg-4 item" data-product="<?php echo $product['product_id']; ?>">

                        <div>




<?php if($product['benefit']) { ?>
                            <div class="sticker"><img src="<?php echo $product['benefit']['image']; ?>" alt="" /><span><?php echo $product['benefit']['name']; ?></span></div>
<?php } ?>
                            <div class="badges">
                                <?php if($product['special_percent']) { ?>
                                <div class="badge badge-sale">-<?php echo $product['special_percent']; ?>%</div>
                                <?php } ?>

                                <?php if($product['new']) { ?>
                                <div class="badge badge-new">NEW</div>
                                <?php } ?>

                                <?php if($product['hit']) { ?>
                                <div class="badge badge-hit">HIT</div>
                                <?php } ?>
                            </div>

                            <div class="image"><button type="button" data-toggle="modal" data-target="#details-<?php echo $product['product_id']; ?>"><img src="<?php echo $product['thumb']; ?>"<?php foreach ($product['dop_img'] as $img) { ?> data-img="<?php echo $img;?>"<?php } ?> alt="<?php echo $product['name']; ?>" /></button></div>

                            <h3><a href="#"><?php echo $product['name']; ?></a></h3>

                            <div class="structure">

                                <ul>

                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(4))) { ?>

                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                    <?php }}} ?>

                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(5))) { ?>

                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                    <?php }}} ?>

                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(6))) { ?>

                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                    <?php }}} ?>

                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(7))) { ?>

                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                    <?php }}} ?>

                                </ul>

                            </div>

                            <div class="info">

                                <?php if ($product['price']) { ?>

                                <div class="price">

                                    <?php if (!$product['special']) { ?>

                                    <?php echo $product['price']; ?>

                                    <?php } else { ?>

                                    <span><?php echo $product['price']; ?></span> <?php echo $product['special']; ?>

                                    <?php } ?>

                                </div>

                                <?php } ?>

                                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn border"><span>В корзину</span></button>

                            </div>

                        </div>

                        <div class="product-details" style="display:none;">

                            <div class="modal fade details-full" id="details-<?php echo $product['product_id']; ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                <div class="modal-dialog modal-dialog-centered" role="document">

                                    <div class="modal-content">

                                        <div class="images">











                                            <div class="main">

                                                <img src="<?php echo $product['thumb']; ?>" id="main-<?php echo $product['product_id']; ?>" alt="<?php echo $product['name']; ?>" />

                                            </div>

                                            <ul class="others">

                                                <li class="active"><a href="<?php echo $product['thumb']; ?>" onclick="swap_<?php echo $product['product_id']; ?>(this); return false;"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?> #1" /></a></li>



                                                <?php foreach ($product['dop_img'] as $img) { ?><li><a href="<?php echo $img;?>" onclick="swap_<?php echo $product['product_id']; ?>(this); return false;"><img src="<?php echo $img;?>" alt="<?php echo $product['name']; ?> #2" /></a></li><?php } ?>









                                            </ul>

                                        </div>









                                        <div class="text">
                                            <div class="text-header">
                                                <?php if($product['benefit']) { ?>
                                                <div class="sticker"><img src="<?php echo $product['benefit']['image']; ?>"
                                                                          alt=""/><span><?php echo $product['benefit']['name']; ?></span>
                                                </div>
                                                <?php } ?>
                                                <div class="badges">
                                                    <?php if($product['special_percent']) { ?>
                                                    <div class="badge badge-sale">-<?php echo $product['special_percent']; ?>%</div>
                                                    <?php } ?>

                                                    <?php if($product['new']) { ?>
                                                    <div class="badge badge-new">NEW</div>
                                                    <?php } ?>

                                                    <?php if($product['hit']) { ?>
                                                    <div class="badge badge-hit">HIT</div>
                                                    <?php } ?>
                                                </div>
                                            </div>


                                            <div class="title"><?php echo $product['name']; ?></div>

                                            <?php if($product['description']) { ?>

                                            <div class="description">

                                                <div id="name_info_in"></div>

                                                <strong>Состав:</strong>

                                                <p><?php echo $product['description']; ?></p>

                                            </div>

                                            <?php } ?>

                                            <?php if($product['weight']) { ?>

                                            <div class="description">

                                                <strong>Вес блюда:</strong>

                                                <p><?php echo $product['weight']; ?> гр.</p>

                                            </div>

                                            <?php } ?>

                                            <script type="text/javascript">

                                                function swap_<?php echo $product['product_id']; ?>(image) {

                                                    document.getElementById("main-<?php echo $product['product_id']; ?>").src = image.href;

                                                }

                                            </script>

                                            <div class="description structure">

                                                <strong>Энергетическая ценность продукта <br />на 100 грамм:</strong>

                                                <ul>

                                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(4))) { ?>

                                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                                    <?php }}} ?>

                                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(5))) { ?>

                                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                                    <?php }}} ?>

                                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(6))) { ?>

                                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                                    <?php }}} ?>

                                                    <?php foreach($product['attribute_groups'] as $attribute_group) {foreach($attribute_group['attribute'] as $attribute) {if(in_array($attribute['attribute_id'], array(7))) { ?>

                                                    <li><span title="<?php echo $attribute['name']; ?>"><?php echo mb_substr($attribute['name'],0,1,'UTF-8'); ?></span> <?php echo $attribute['text']; ?></li>

                                                    <?php }}} ?>

                                                </ul>

                                            </div>

                                            <div class="info">

                                                <div class="price">

                                                    <?php if (!$product['special']) { ?>

                                                    <?php echo $product['price']; ?>

                                                    <?php } else { ?>

                                                    <span><?php echo $product['price']; ?></span> <?php echo $product['special']; ?>

                                                    <?php } ?>

                                                </div>

                                                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');" class="btn border"><span>В корзину</span></button>

                                            </div>

                                        </div>

                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class="">

									<g>

                                        <path xmlns="http://www.w3.org/2000/svg" d="m256 512c-141.160156 0-256-114.839844-256-256s114.839844-256 256-256 256 114.839844 256 256-114.839844 256-256 256zm0-475.429688c-120.992188 0-219.429688 98.4375-219.429688 219.429688s98.4375 219.429688 219.429688 219.429688 219.429688-98.4375 219.429688-219.429688-98.4375-219.429688-219.429688-219.429688zm0 0" fill="#232224" data-original="#000000" style="" class=""/>

                                        <path xmlns="http://www.w3.org/2000/svg" d="m347.429688 365.714844c-4.679688 0-9.359376-1.785156-12.929688-5.359375l-182.855469-182.855469c-7.144531-7.144531-7.144531-18.714844 0-25.855469 7.140625-7.140625 18.714844-7.144531 25.855469 0l182.855469 182.855469c7.144531 7.144531 7.144531 18.714844 0 25.855469-3.570313 3.574219-8.246094 5.359375-12.925781 5.359375zm0 0" fill="#232224" data-original="#000000" style="" class=""/>

                                        <path xmlns="http://www.w3.org/2000/svg" d="m164.570312 365.714844c-4.679687 0-9.355468-1.785156-12.925781-5.359375-7.144531-7.140625-7.144531-18.714844 0-25.855469l182.855469-182.855469c7.144531-7.144531 18.714844-7.144531 25.855469 0 7.140625 7.140625 7.144531 18.714844 0 25.855469l-182.855469 182.855469c-3.570312 3.574219-8.25 5.359375-12.929688 5.359375zm0 0" fill="#232224" data-original="#000000" style="" class=""/>

                                    </g>

								</svg>

                                        </button>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <?php } ?>

                </div>

            </section>
            <?php } ?>
        </main>
        <aside id="sidebar" class="col-xl-3 d-none d-xl-block">

        </aside>
    </div>
</div>

<script>
    var text = document.querySelector('#header .cart-details').innerHTML,
        sidebar = 	document.querySelector('#sidebar');

    if(sidebar) document.querySelector('#sidebar').innerHTML = text;

    if(['#by_new', '#by_hit', '#by_price_up', '#by_price_down', '#by_name'].indexOf(window.location.hash) >= 0) {
        initSort(window.location.hash);
    } else {
        initSort('#by_name');
    }

    $('#sort li a').click(function(e){
       e.preventDefault();

       initSort($(this).attr('href'))
    });

    function initSort(name) {
        $('#sort li').removeClass('active');
        $('#sort li a[href="' + name + '"]').parent().addClass('active');

        name = name.replace('#', '');

        $('#products .list').each(function(){
            var sorts = $(this).data('sorts'),
                sort = sorts[name],
                $items = $(this).find('.list-items');

            for(var i in sort) {
                $items.append($items.find('[data-product="' + sort[i] + '"]'));
            }
        });
    }
</script>