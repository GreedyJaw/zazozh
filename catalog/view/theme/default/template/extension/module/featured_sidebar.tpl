<div class="popular-sidebar">
    <div class="title"><?php echo $heading_title; ?></div>
    <div class="list">
        <?php foreach ($products as $product) { ?>
        <div class="item">
            <div class="image"><img src="<?php echo $product['thumb']; ?>"<?php foreach ($product['dop_img'] as $img) { ?> data-img="<?php echo $img;?>"<?php } ?> alt="<?php echo $product['name']; ?>" /></div>
            <div class="text">
                <div class="badges">
                    <?php if($product['special_percent']) { ?>
                    <div class="badge badge-sale">-<?php echo $product['special_percent']; ?>%</div>
                    <?php } ?>

                    <?php if($product['new']) { ?>
                    <div class="badge badge-new">NEW</div>
                    <?php } ?>

                    <?php if($product['hit']) { ?>
                    <div class="badge badge-hit">HIT</div>
                    <?php } ?>
                </div>
                <h3><?php echo $product['name']; ?></h3>
                <div class="price"><?php echo $product['special'] ?: $product['price']; ?></div>
                <div class="button">
                    <button type="button" class="btn border" onclick="cart.add('<?php echo $product['product_id']; ?>');"><span>В корзину</span></button>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>