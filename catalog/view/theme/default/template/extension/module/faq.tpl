<section class="faq">
    <div class="container">
        <header><span><?php echo $heading_title; ?></span></header>
        <div id="faq">
            <div class="row">
                <div id="faq-left" class="col-md-6 left">
                    <?php for($i = 0; $i < count($items); $i++) { if($i%2===0) { ?>
                    <div class="card">
                        <div class="card-header" id="faq-left-header-<?php echo $i; ?>">
                            <button data-toggle="collapse" data-target="#faq-left-content-<?php echo $i; ?>" aria-expanded="true" aria-controls="faq-left-1"><?php echo $items[$i]['title']; ?></button>
                        </div>
                        <div id="faq-left-content-<?php echo $i; ?>" class="collapse show" aria-labelledby="faq-left-1" data-parent="#faq-left">
                            <div class="card-body"><?php echo $items[$i]['text']; ?></div>
                        </div>
                    </div>
                    <?php } } ?>
                </div>

                <div id="faq-right" class="col-md-6 right">
                    <?php for($i = 1; $i < count($items); $i++) { if($i%2!==0) { ?>
                    <div class="card">
                        <div class="card-header" id="faq-right-header-<?php echo $i; ?>">
                            <button class="collapsed" data-toggle="collapse" data-target="#faq-right-content-<?php echo $i; ?>" aria-expanded="false" aria-controls="faq-right-1"><?php echo $items[$i]['title']; ?></button>
                        </div>
                        <div id="faq-right-content-<?php echo $i; ?>" class="collapse" aria-labelledby="faq-right-1" data-parent="#faq-right">
                            <div class="card-body"><?php echo $items[$i]['text']; ?></div>
                        </div>
                    </div>
                    <?php } } ?>
                </div>
            </div>
        </div>
    </div>
</section>