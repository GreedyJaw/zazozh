
    <section id="slider-<?php echo $module; ?>" class="slider">
        <div class="container">
            <div class="slider-list owl-carousel">

                <?php foreach ($banners as $banner) { ?>
                <div class="slide">
                    <?php if ($banner['link']) { ?>
                    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
                    <?php } else { ?>
                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                    <?php } ?>
                </div>
                <?php } ?>

            </div>
            <div class="slider-navigation"></div>
        </div>
    </section>


<script>
    $('.slider-list').owlCarousel({
        //autoplay:true,
        // autoplayTimeout: 3000,
        // autoplaySpeed: 500,
        // autoplayHoverPause: true,
        // autoWidth:true,
        center: true,
        items:1,
        loop: true,
        // lazyLoad: true,
        margin: 20,
        nav: true,
        navContainer: '.slider-navigation',
        navText: ['<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 7.29289C-0.0976295 7.68341 -0.0976296 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31945 8.07107 0.928929C7.68054 0.538404 7.04738 0.538404 6.65685 0.928929L0.292893 7.29289ZM46 7L1 7L1 9L46 9L46 7Z" /></svg>', '<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M45.7071 8.70711C46.0976 8.31658 46.0976 7.68342 45.7071 7.29289L39.3431 0.928932C38.9526 0.538408 38.3195 0.538408 37.9289 0.928932C37.5384 1.31946 37.5384 1.95262 37.9289 2.34315L43.5858 8L37.9289 13.6569C37.5384 14.0474 37.5384 14.6805 37.9289 15.0711C38.3195 15.4616 38.9526 15.4616 39.3431 15.0711L45.7071 8.70711ZM0 9H45V7H0V9Z" /></svg>'],
        dots: true,
        // responsiveClass: true
    });

</script>


