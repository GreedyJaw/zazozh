<main id="blog">
	<section class="category featured">
		<div class="container">
<header><span><?php echo $heading_title; ?></span></header>
			<div class="articles-slider">
				<?php if ($articles) { ?>
				<div class="articles-list owl-carousel">
					<?php foreach ($articles as $article) { ?>
					<div class="item">
						<div>
							<div class="image">
								<img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" />
								<div class="view" title="<?php echo $text_views; ?> "><?php echo $article["viewed"];?></div>
							</div>
							<div class="text">
								<h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>
								<p><?php echo $article['description']; ?></p>
								<div class="button"><a href="<?php echo $article['href']; ?>" class="btn border">Читать полностью</a></div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="articles-navigation"></div>
			</div>
			<div class="more text-center"><a href="/blog/" class="btn green">Перейти в блог</a></div>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>			
		</div>
	</section>
</main>

<script>
	$('.articles-list').owlCarousel({

		//autoplay:true,

		autoplayTimeout: 3000,

		autoplaySpeed: 500,

		autoplayHoverPause: true,

		items:4,

		loop: $('.articles-list').children().length > 4,

		lazyLoad: true,

		margin: 30,

		nav: true,

		navContainer: '.articles-navigation',

		navText: ['<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 7.29289C-0.0976295 7.68341 -0.0976296 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31945 8.07107 0.928929C7.68054 0.538404 7.04738 0.538404 6.65685 0.928929L0.292893 7.29289ZM46 7L1 7L1 9L46 9L46 7Z" /></svg>', '<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M45.7071 8.70711C46.0976 8.31658 46.0976 7.68342 45.7071 7.29289L39.3431 0.928932C38.9526 0.538408 38.3195 0.538408 37.9289 0.928932C37.5384 1.31946 37.5384 1.95262 37.9289 2.34315L43.5858 8L37.9289 13.6569C37.5384 14.0474 37.5384 14.6805 37.9289 15.0711C38.3195 15.4616 38.9526 15.4616 39.3431 15.0711L45.7071 8.70711ZM0 9H45V7H0V9Z" /></svg>'],

		dots: false,

		responsiveClass: true,

		responsive: {
			0: {
				items: 1,
				loop: true,
				dots: true
			},
			480: {
				items: 2,
				loop: $('.articles-list').children().length > 2,
				margin: 10,
				dots: true
			},
			576: {
				items: 2,
				loop: $('.articles-list').children().length > 2,
				margin: 30,
				dots: true
			},
			768: {
				items: 3,
				loop: $('.articles-list').children().length > 3
			},
			1199: {
				items: 4,
				loop: $('.articles-list').children().length > 4
			}
		}

	});
</script>
