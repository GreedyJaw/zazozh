<section class="press">
    <div class="container">
        <header><span><?php echo $heading_title; ?></span></header>
        <div class="press-slider">
            <div class="press-list owl-carousel">
                <?php foreach($items as $item) { ?>
                <div class="item">
                    <div class="image">
                        <img src="<?php echo $item['thumb']; ?>" alt="<?php echo $item['title']; ?>" />
                    </div>
                    <div class="text">
                        <p><strong><?php echo $item['title']; ?></strong></p>
                        <p><?php echo $item['text']; ?></p>
                        <p><a href="<?php echo $item['source_link']; ?>" target="_blank">Полная версия статьи на <?php echo $item['source_name']; ?></a></p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="press-navigation"></div>
        </div>
    </div>
</section>

<script>
    $('.press-list').owlCarousel({
        //autoplay:true,
        autoplayTimeout: 3000,
        autoplaySpeed: 500,
        autoplayHoverPause: true,
        items: 2,
        loop: $('.press-list').children().length > 2,
        lazyLoad: true,
        margin: 30,
        nav: true,
        navContainer: '.press-navigation',
        navText: ['<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 7.29289C-0.0976295 7.68341 -0.0976296 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31945 8.07107 0.928929C7.68054 0.538404 7.04738 0.538404 6.65685 0.928929L0.292893 7.29289ZM46 7L1 7L1 9L46 9L46 7Z" /></svg>', '<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M45.7071 8.70711C46.0976 8.31658 46.0976 7.68342 45.7071 7.29289L39.3431 0.928932C38.9526 0.538408 38.3195 0.538408 37.9289 0.928932C37.5384 1.31946 37.5384 1.95262 37.9289 2.34315L43.5858 8L37.9289 13.6569C37.5384 14.0474 37.5384 14.6805 37.9289 15.0711C38.3195 15.4616 38.9526 15.4616 39.3431 15.0711L45.7071 8.70711ZM0 9H45V7H0V9Z" /></svg>'],
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                loop: $('.press-list').children().length > 1,
                dots: true
            },
            992: {
                items: 2,
                loop: $('.press-list').children().length > 2
            },
            1199: {
                items: 2,
                loop: $('.press-list').children().length > 2
            }
        }
    });
</script>