<div class="list-group" id="account-menu">
  <a href="<?php echo $edit; ?>" class="list-group-item <?php echo $route === 'account/edit' ? 'current' : ''; ?>"><?php echo $text_edit; ?></a>
  <a href="<?php echo $password; ?>" class="list-group-item <?php echo $route === 'account/password' ? 'current' : ''; ?>"><?php echo $text_password; ?></a>
  <a href="<?php echo $address; ?>" class="list-group-item <?php echo $route === 'account/address' || $route === 'account/address/edit' || $route === 'account/address/add' ? 'current' : ''; ?>"><?php echo $text_address; ?></a>
  <a href="<?php echo $order; ?>" class="list-group-item <?php echo $route === 'account/order' || $route === 'account/order/info' ? 'current' : ''; ?>"><?php echo $text_order; ?></a>
  <a href="<?php echo $newsletter; ?>" class="list-group-item <?php echo $route === 'account/newsletter' ? 'current' : ''; ?>"><?php echo $text_newsletter; ?></a>
  <a href="<?php echo $logout; ?>" class="list-group-item"><?php echo $text_logout; ?></a>
</div>
