<?php echo $header; ?>
<main id="content">
	<section class="main-title">
		<div class="container">
			<header>
				<h1><?php echo $heading_title; ?></h1>
			</header>
			<div class="breadcrumb">
				<ul>
					<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<li><?php if($i+1<count($breadcrumbs)) { ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <?php } else { ?><span><?php echo $breadcrumb['text']; ?></span><?php } ?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</section>
</main>
<main id="contacts">
	<section class="page">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 text">
					<div class="row">
						<div class="col-sm-6 phone">
							<div class="name">Наши телефоны:</div>
							<a href="tel:<?php echo str_replace(array('(', ')', ' ', '-'), '', $telephone); ?>"><?php echo $telephone; ?></a>
						</div>
						<div class="col-sm-6 mail">
							<div class="name">Наш e-mail:</div>
							<a href="mailto:<?php echo $mail_c; ?>"><?php echo $mail_c; ?></a>
						</div>
						<div class="col-lg-12 location">
							<div class="name">Фактический адрес / юридический адрес:</div>
							<p><?php echo $address; ?></p>
						</div>
						<div class="col-lg-12 requisites">
							<div>
								<div class="name">Наши реквизиты:</div>
								<p><strong>Общество с ограниченной ответственностью &laquo;ФудСервис&raquo;</strong></p>
								<div class="left">
									<p>ИНН 7810903116</p>
									<p>КПП 781001001</p>
								</div>
								<div class="right">
									<p>ОГРН 1207800116743</p>
									<p>ОКПО 45561693</p>
								</div>
								<p><strong>Регистрирующий орган:</strong></p>
								<p>Межрайонная инспекция ФНС России №23 по Санкт-Петербургу</p>
								<div class="left">
									<p><strong>Дата регистрации:</strong></p>
									<p>15.09.2020</p>
								</div>
								<div class="right">
									<p><strong>Генеральный директор:</strong></p>
									<p>Николкина Ксения Николаевна</p>
								</div>
							</div>
						</div>
						<div class="col-lg-12 bank">
							<p><strong>ТОЧКА ПАО БАНКА "ФК ОТКРЫТИЕ", г. Москва</strong></p>
							<p>р/с: 40702810003500026632, БИК 044525999, к/с: 30101810845250000999</p>
						</div>
						<div class="col-lg-12 boss">
							<div>
								<div class="left"><img src="../catalog/view/theme/default/image/boss.png" alt="Ламбер Михаил Вильямович" /></div>
								<div class="right">
									<div>
										<p>Основатель zazozh.pro</p>
										<div class="user">Ламбер Михаил Вильямович</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 form mt-4 mt-lg-0">
					<div>
						<form method="post" enctype="multipart/form-data" id="contactForm">
							<div class="title">
								<strong>Напишите нам</strong> мы ответим на все Ваши вопросы <br />в ближайшее время
							</div>
							<div class="content">
								<div class="input user">
									<input type="text" name="name" id="input-name" placeholder="Ваше имя" />
								</div>
								<div class="input mail">
									<input type="text" name="email" id="input-email" placeholder="E-mail" />
								</div>
								<div class="input message">
									<textarea name="enquiry" rows="10" id="input-enquiry" placeholder="Ваше сообщение"></textarea>
								</div>
							</div>
							<?php echo $captcha; ?>

							<div class="text-center">
								<input class="btn green" type="submit" value="Применить">
								<div class="input checkbox mt-3">
									<input type="checkbox" name="agree" value="1" id="privacy-policy" checked="checked" />
									<label for="privacy-policy">Я даю согласие на обработку <br>персональных данных</label>
								</div>
							</div>

						</form>
					</div>
				</div>
				<div class="col-lg-12 map">
					<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
	</section>
</main>

<script>
	$('#contactForm').submit(function(e){
		e.preventDefault();

		var $form = $(this);

		$form.find('.text-danger').remove();

		$.post('index.php?route=information/contact/send', $form.serialize(), function(json){
			console.log(json);

			if(json.errors) {
				for(var key in json.errors) {
					$form.find('[name="' + key + '"]').after('<div class="text-danger">' + json.errors[key] + '</div>');
				}
			}

			if(json.success) {
				$('#successModal .modal-content p').text('Заявка успешно отправлена');
				$('#successModal').modal('show');
				$form.trigger('reset');
			}

		}, 'json').fail(function(err){
			console.log(err);
		});
	});
</script>

<?php echo $footer; ?>