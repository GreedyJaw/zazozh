<?php echo $header; ?>
<main id="content">
	<section class="main-title">
		<div class="container">
			<header>
				<h1><?php echo $heading_title; ?></h1>
			</header>
			<div class="breadcrumb">
				<ul>
					<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<li><?php if($i+1<count($breadcrumbs)) { ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <?php } else { ?><span><?php echo $breadcrumb['text']; ?></span><?php } ?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</section>
</main>
<main<?php if ($_SERVER['REQUEST_URI'] == "/delivery-payment/") { ?> id="process" class="delivery-payment"<?php } ?><?php if ($_SERVER['REQUEST_URI'] == "/about/") { ?> id="page" class="about"<?php } ?>>
	<?php echo $content_top; ?>
	<section class="page">
		<div class="container">
			<?php echo $description; ?>
		</div>
	</section>

<?php if ($_SERVER['REQUEST_URI'] == "/about/") { ?>


	<?php echo $content_bottom; ?><?php } ?>
</main>
<?php echo $footer; ?>