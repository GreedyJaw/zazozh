<?php echo $header; ?>

<main id="content">
    <section class="main-title">
        <div class="container">
            <header>
                <h1>Оформление заказа</h1>
            </header>
            <div class="breadcrumb">
                <ul>
                    <li><a href="/"><span>Главная</span></a> </li>
                    <li><a href="/cart"><span>Корзина покупок</span></a> </li>
                    <li><span>Оформление заказа</span></li>
                </ul>
            </div>
        </div>
    </section>
</main>

<div class="container">
    <div class="row">
        <?php echo $column_left; ?>

        <?php if ($column_left) { ?>
        <?php $class = 'col-lg-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-12'; ?>
        <?php } ?>

        <main class="<?php echo $class; ?>">
            <div class="success-block">
                <h1>Ваш заказ принят!</h1>
                <p>В ближайшее время мы свяжемся с Вами для уточнения деталей заказа</p>
                <a href="/" class="btn green">Вернуться на главную</a>
            </div>
        </main>
    </div>
</div>
<?php echo $footer; ?>