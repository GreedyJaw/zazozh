<?php echo $header; ?>
<main id="content">
    <section class="main-title">
        <div class="container">
            <header>
                <h1><?php echo $heading_title; ?></h1>
            </header>
            <div class="breadcrumb">
                <ul>
                    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li><?php if($i+1<count($breadcrumbs)) { ?><a
                                href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <?php } else { ?>
                        <span><?php echo $breadcrumb['text']; ?></span><?php } ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
</main>


<div class="container">

    <?php if ($attention) { ?>
    <div class="alert alert-info"><?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <div class="row flex-column-reverse flex-xl-row">
        <?php echo $column_left; ?>
        <main id="basket" class="col-xl-9">
            <section class="page">

                <?php if($products) { ?>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">


                    <div class="table">
                        <table cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <td>Изображение</td>
                                <td>Наименование</td>
                                <td>Количество</td>
                                <td>Цена за ед.</td>
                                <td>Сумма</td>
                                <td>Удалить</td>
                            </tr>
                            </thead>
                            <tbody>


                            <?php foreach ($products as $product) { ?>

                            <tr>
                                <td class="image"><img src="<?php echo $product['thumb']; ?>"
                                                       alt="<?php echo $product['name']; ?>"
                                                       title="<?php echo $product['name']; ?>"/>


                                </td>
                                <td class="name"><strong><?php echo $product['name']; ?></strong> <span>Вес блюда: <ins><?php echo $product['weight']; ?>
                                            гр.</ins></span></td>
                                <td class="quantity">
                                    <div>
                                        <button type="button" class="minus">–</button>
                                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]"
                                               value="<?php echo $product['quantity']; ?>" size="1"
                                               class="form-control"/>
                                        <button type="button" class="plus">+</button>
                                    </div>
                                </td>
                                <td class="price"><?php echo $product['price']; ?></td>
                                <td class="total"><?php echo $product['total']; ?></td>
                                <td class="remove">
                                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>"
                                            onclick="cart.remove('<?php echo $product['cart_id']; ?>', true);"><span>Удалить</span>
                                    </button>

                                </td>
                            </tr>

                            <?php } ?>


                            </tbody>
                        </table>
                    </div>


                </form>


                <div class="order-info">
                    <?php foreach ($totals as $total) { if($total['code'] !== 'total') { ?>
                    <div><strong><?php echo $total['title']; ?>:</strong> <span><?php echo $total['text']; ?></span></div>
                    <?php } } ?>
                </div>

                <?php foreach ($totals as $total) { if($total['code'] === 'total') { ?>
                <div class="order-info total">
                    <div><strong><?php echo $total['title']; ?>:</strong> <span><?php echo $total['text']; ?></span></div>
                </div>
                <?php } } ?>


                <div class="buttons">
                    <a href="/" class="btn border">Вернуться к покупкам</a>
                    <a href="<?php echo $checkout; ?>" class="btn green">Оформить заказ</a>
                </div>
                <?php } else { ?>
                <h2>В корзине нет товаров</h2>
                <a href="/" class="btn green">На главную</a>
                <?php } ?>
            </section>
        </main>
    </div>
</div>


<script>

    if ($(window).width() < 768) {
        $('.table tbody tr').each(function () {
            $('.table').append('<div class="item">' +
                '<div class="remove">' + $('.remove', this).html() + '</div>' +
                '<div class="image">' + $('.image', this).html() + '</div>' +
                '<div class="text">' +
                '	<div class="name">' + $('.name', this).html() + '</div>' +
                '	<div class="quantity">' + $('.quantity', this).html() + '</div>' +
                '   <div class="prices">' +
                '		<div class="price"><label>Цена за ед.</label><br>' + $('.price', this).html() + '</div>' +
                '       <div class="total"><label>Сумма</label><br>' + $('.total', this).html() + '</div>' +
                '	</div>' +
                '</div>' +
                '</div>');
        });

        $('.table table').remove();
    }
</script>

<?php echo $footer; ?>
