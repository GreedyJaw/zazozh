<div class="row">
  <div class="col-sm-6 inputs-group radio mb-4 mb-md-0">
    <h3 class="input-group-title">Новый пользователь</h3>
    <p>Варианты оформления заказа</p>
    <div class="list">
      <div class="input radio">
        <?php if ($account == 'register') { ?>
        <input type="radio" name="account" id="register_radio" value="register" checked="checked">
        <?php } else { ?>
        <input type="radio" name="account" id="register_radio" value="register">
        <?php } ?>
        <label for="register_radio">Зарегистрироваться</label>
      </div>
      <?php if ($checkout_guest) { ?>
      <div class="input radio">
        <?php if ($account == 'guest') { ?>
        <input type="radio" name="account" id="guest_radio" value="guest" checked="checked" />
        <?php } else { ?>
        <input type="radio" name="account" id="guest_radio" value="guest" />
        <?php } ?>
        <label for="guest_radio">Заказать без регистрации</label>
      </div>
      <?php } ?>
      <p>Создав учетную запись, вы сможете совершать покупки быстрее, быть в курсе статуса заказа и отслеживать заказы, которые вы ранее сделали.</p>
      <input type="button" value="Продолжить" id="button-account" class="btn green">
    </div>
  </div>
  <div class="col-sm-6 inputs-group radio" id="checkout-login-form">
    <h3 class="input-group-title">Постоянный покупатель</h3>
    <p>Я постоянный покупатель</p>
    <div class="inputs-group">
      <div class="input">
        <label class="control-label" for="input-email">E-Mail</label>
        <input type="text" name="email" value="" placeholder="example@gmail.com" id="input-email" class="form-control">
      </div>
      <div class="input">
        <label class="control-label" for="input-password">Пароль</label>
        <input type="password" name="password" value="" placeholder="*****" id="input-password" class="form-control">
      </div>
    </div>
    <div class="forgot"><a href="#" data-target="#forgottenModal" data-toggle="modal">Забыли пароль?</a></div>
    <input type="button" value="Войти" id="button-login" class="btn green">
  </div>
</div>

<div class="modal modal-form" id="forgottenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <?php echo $forgottenForm; ?>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">

        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
             xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="512" height="512" x="0" y="0"
             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class="">

        									<g>

                                              <path xmlns="http://www.w3.org/2000/svg"
                                                    d="m256 512c-141.160156 0-256-114.839844-256-256s114.839844-256 256-256 256 114.839844 256 256-114.839844 256-256 256zm0-475.429688c-120.992188 0-219.429688 98.4375-219.429688 219.429688s98.4375 219.429688 219.429688 219.429688 219.429688-98.4375 219.429688-219.429688-98.4375-219.429688-219.429688-219.429688zm0 0"
                                                    fill="#232224" data-original="#000000" style="" class=""></path>

                                              <path xmlns="http://www.w3.org/2000/svg"
                                                    d="m347.429688 365.714844c-4.679688 0-9.359376-1.785156-12.929688-5.359375l-182.855469-182.855469c-7.144531-7.144531-7.144531-18.714844 0-25.855469 7.140625-7.140625 18.714844-7.144531 25.855469 0l182.855469 182.855469c7.144531 7.144531 7.144531 18.714844 0 25.855469-3.570313 3.574219-8.246094 5.359375-12.925781 5.359375zm0 0"
                                                    fill="#232224" data-original="#000000" style="" class=""></path>

                                              <path xmlns="http://www.w3.org/2000/svg"
                                                    d="m164.570312 365.714844c-4.679687 0-9.355468-1.785156-12.925781-5.359375-7.144531-7.140625-7.144531-18.714844 0-25.855469l182.855469-182.855469c7.144531-7.144531 18.714844-7.144531 25.855469 0 7.140625 7.140625 7.144531 18.714844 0 25.855469l-182.855469 182.855469c-3.570312 3.574219-8.25 5.359375-12.929688 5.359375zm0 0"
                                                    fill="#232224" data-original="#000000" style="" class=""></path>

                                            </g>

        								</svg>

      </button>
    </div>
  </div>
</div>
