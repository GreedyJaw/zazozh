<?php if ($error_warning) { ?>
<div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<p><?php echo $text_payment_method; ?></p>
<?php foreach ($payment_methods as $payment_method) { ?>
<div class="input radio">
    <?php if ($payment_method['code'] == $code || !$code) { ?>
    <?php $code = $payment_method['code']; ?>
    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" checked="checked" id="payment-method<?php echo $payment_method['code']; ?>" />
    <?php } else { ?>
    <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="payment-method<?php echo $payment_method['code']; ?>" />
    <?php } ?>
    <label id="payment-method<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?>
      <?php if ($payment_method['terms']) { ?>
      (<?php echo $payment_method['terms']; ?>)
      <?php } ?></label>

</div>
<?php } ?>
<?php } ?>

<textarea name="comment" rows="8" class="form-control" id="payment-method-comment" style="display:none;"><?php echo $comment; ?></textarea>

<?php if ($text_agree) { ?>
<div class="buttons">
  <div class="agree-block pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn green" />
    <div class="input checkbox">
      <input type="checkbox" name="agree" value="1" id="payment-agree" checked />
      <label for="payment-agree">Я даю согласие на обработку персональных данных</label>
    </div>
  </div>
</div>
<?php } else { ?>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary" />
  </div>
</div>
<?php } ?>
