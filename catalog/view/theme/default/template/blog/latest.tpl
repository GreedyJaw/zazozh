<?php echo $header; ?>
<main id="content">
	<section class="main-title">
		<div class="container">
			<header>
				<h1><?php echo $heading_title; ?></h1>
			</header>
			<div class="breadcrumb">
				<ul>
					<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<li><?php if($i+1<count($breadcrumbs)) { ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a> <?php } else { ?><span><?php echo $breadcrumb['text']; ?></span><?php } ?></li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</section>
</main>
<main id="blog">
	<section class="category">
		<div class="container">
			<?php if ($articles) { ?>
			<div class="row" id="articles">
				<?php foreach ($articles as $article) { ?>     
				<div class="col-6 col-md-4 col-xl-3 item">
					<div>
						<div class="image">
							<img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" />
							<div class="view" title="<?php echo $text_views; ?> "><?php echo $article["viewed"];?></div>
						</div>
						<div class="text">
							<h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>
							<p><?php echo $article['description']; ?></p>
							<div class="button"><a href="<?php echo $article['href']; ?>" class="btn border">Читать полностью</a></div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
			<?php } else { ?>
			<p><?php echo $text_empty; ?></p>
			<div class="buttons">
				<div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
			</div>
			<?php } ?>

			<?php if($pages_total > 1) { ?>
			<div class="more text-center"><button type="button" class="btn green" id="loadmore">Загрузить еще</button></div>
			<?php } ?>
		</div>
	</section>
</main>
<?php echo $footer; ?>

<script>
	var page = parseInt('<?php echo $page; ?>'),
		pagesTotal = parseInt('<?php echo $pages_total; ?>'),
		limit = parseInt('<?php echo $limit; ?>');

	page++;

	$('#loadmore').click(function(){
		$.get('index.php?route=blog/latest/loadmore&page=' + page, null, function(html) {
			$('#articles').append(html);

			if(page === pagesTotal) $('#loadmore').remove();
		});
	});
</script>