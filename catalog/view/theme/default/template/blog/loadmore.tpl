<?php foreach ($articles as $article) { ?>
<div class="col-6 col-md-4 col-xl-3 item">
    <div>
        <div class="image">
            <img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" />
            <div class="view" title="<?php echo $text_views; ?> "><?php echo $article["viewed"];?></div>
        </div>
        <div class="text">
            <h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>
            <p><?php echo $article['description']; ?></p>
            <div class="button"><a href="<?php echo $article['href']; ?>" class="btn border">Читать полностью</a></div>
        </div>
    </div>
</div>
<?php } ?>