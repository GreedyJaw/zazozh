<?php echo $header; ?>
<main id="error-404">
	<section class="page">
		<div class="container">
			<header>
				<h1>Ошибка 404. Страница не найдена!</h1>
			</header>
			<div class="row">
				<div class="col-12 text">
					<img src="catalog/view/theme/default/image/404.png" alt="<?php echo $heading_title; ?>" />
					<div class="button text-center"><a href="/" class="btn green">Вернуться на главную</a></div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php echo $footer; ?>