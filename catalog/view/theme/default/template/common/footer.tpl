<footer id="footer">
    <div class="container">
        <div class="top">
            <div class="row">
                <div class="col-xl-auto col-lg-3 col-md-6 logo order-4 order-lg-0">
                    <div>
                        <?php if(!$is_home) { ?>
                        <a href="/"><img src="catalog/view/theme/default/image/logo-footer.png"/></a>
                        <?php } else { ?>
                        <img src="catalog/view/theme/default/image/logo-footer.png"/>
                        <?php } ?>
                    </div>
                    <p><?php echo $powered; ?>
                    <p>
                </div>
                <div class="col-xl-auto col-lg-3 col-md-4 col-sm-6 links">
                    <div class="title">Каталог</div>
                    <ul id="footer-catalog">
                        <?php foreach($categories as $category) { ?>
                        <li>
                            <a href="#category<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="col-xl-4 col-lg-3 col-md-4 col-sm-6 links others">
                    <div class="title">Разделы сайта</div>
                    <ul>
                        <li><a href="/about/">О нас</a></li>
                        <li><a href="/blog/">Блог</a></li>
                        <li><a href="/contacts/">Контакты</a></li>
                        <li><a href="#">Франшиза</a></li>
                        <li><a href="/delivery-payment/">Доставка и оплата</a></li>
                    </ul>
                </div>
                <div class="col-xl-auto col-lg-3 col-md-4 contacts order-3 order-lg-0 mt-4 mt-md-0">
                    <div class="phone"><a
                                href="tel:<?php echo str_replace(array('(', ')', ' ', '-'), '', $telephone); ?>"><?php echo $telephone; ?></a>
                    </div>
                    <div class="mail"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
                    <div class="socials">
                        <ul>
                            <?php if($vk) { ?>
                            <li class="vk"><a href="<?php echo $vk; ?>" target="_blank" class="fa fa-vk"><span>ВКонтакте</span></a>
                            </li>
                            <?php } ?>

                            <?php if($ok) { ?>
                            <li class="ok"><a href="<?php echo $ok; ?>" target="_blank"
                                              class="fa fa-odnoklassniki"><span>Одноклассники</span></a></li>
                            <?php } ?>

                            <?php if($fb) { ?>
                            <li class="facebook"><a href="<?php echo $fb; ?>" target="_blank" class="fa fa-facebook-f"><span>Facebook</span></a>
                            </li>
                            <?php } ?>

                            <?php if($insta) { ?>
                            <li class="instagram"><a href="<?php echo $insta; ?>" target="_blank"
                                                     class="fa fa-instagram"><span>Instagram</span></a></li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php if($privacy) { ?>
        <div class="bottom">
            <a href="<?php echo $privacy; ?>" target="_blank">Политика обработки персональных данных</a>
        </div>
        <?php } ?>
    </div>
</footer>
<div id="modals"></div>
<script src="catalog/view/javascript/bootstrap.min.js"></script>
<script src="catalog/view/javascript/custom.js"></script>

<script>
    $('.product-details').contents().appendTo('#modals');
</script>

<?php if ($_SERVER['REQUEST_URI'] == "/about/") { ?>
<script>
    $('.tiktok-list').owlCarousel({
        //autoplay:true,
        autoplayTimeout: 3000,
        autoplaySpeed: 500,
        autoplayHoverPause: true,
        items: 4,
        loop: true,
        lazyLoad: true,
        margin: 30,
        nav: true,
        navContainer: '.tiktok-navigation',
        navText: ['<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.292893 7.29289C-0.0976295 7.68341 -0.0976296 8.31658 0.292892 8.7071L6.65685 15.0711C7.04738 15.4616 7.68054 15.4616 8.07107 15.0711C8.46159 14.6805 8.46159 14.0474 8.07107 13.6569L2.41422 8L8.07107 2.34314C8.46159 1.95262 8.46159 1.31945 8.07107 0.928929C7.68054 0.538404 7.04738 0.538404 6.65685 0.928929L0.292893 7.29289ZM46 7L1 7L1 9L46 9L46 7Z" /></svg>', '<svg width="46" height="16" viewBox="0 0 46 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M45.7071 8.70711C46.0976 8.31658 46.0976 7.68342 45.7071 7.29289L39.3431 0.928932C38.9526 0.538408 38.3195 0.538408 37.9289 0.928932C37.5384 1.31946 37.5384 1.95262 37.9289 2.34315L43.5858 8L37.9289 13.6569C37.5384 14.0474 37.5384 14.6805 37.9289 15.0711C38.3195 15.4616 38.9526 15.4616 39.3431 15.0711L45.7071 8.70711ZM0 9H45V7H0V9Z" /></svg>'],
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                dots: true
            },
            480: {
                items: 2,
                margin: 10,
                dots: true
            },
            576: {
                items: 2,
                margin: 30,
                dots: true
            },
            768: {
                items: 3
            },
            1200: {
                items: 4
            }
        }
    });
</script>
<?php } ?>


</body>
</html>