<?php echo $header; ?>

<main id="content">
  <section class="main-title">
    <div class="container">
      <header>
        <h1>Личный кабинет</h1>
      </header>
      <div class="breadcrumb">
        <ul>
          <li><a href="/"><span>Главная</span></a> </li>
          <li><span>Личный кабинет</span></li>
        </ul>
      </div>
    </div>
  </section>
</main>

<div class="container">
  <div class="row">
    <?php echo $column_left; ?>

    <?php if ($column_left) { ?>
    <?php $class = 'col-lg-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12'; ?>
    <?php } ?>

    <main class="<?php echo $class; ?>">
      <div class="success-block">
        <h1>Выход</h1>
        <p>Вы успешно вышли из личного кабинета</p>
        <a href="/" class="btn green">Вернуться на главную</a>
      </div>
    </main>
  </div>
</div>
<?php echo $footer; ?>