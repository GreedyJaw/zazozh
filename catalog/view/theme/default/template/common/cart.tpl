<div id="cart" class="cart"><a href="/cart/">Корзина <span id="cart-total"><?php echo $text_items; ?></span></a></div>
<div style="position:absolute;top:0;left:0;display:none;">
	<div class="cart-details">
		<div>
			<div class="content open">
				<div class="name">
					<div class="title">Корзина (<?php echo $text_items; ?>)</div>

					<?php if($products) { ?>
					<div class="clear"><a href="#">очистить</a></div>
					<?php } ?>
				</div>
				<div class="list">
					<?php if ($products || $vouchers) { ?>
					<?php foreach ($products as $product) { ?>
					<div class="item">
						<?php if ($product['thumb']) { ?>
						<div class="image">
							<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
						</div>
						<?php } ?>
						<div class="text">
							<div class="info">
								<p><?php echo $product['name']; ?></p>
								<button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>"><span><?php echo $button_remove; ?></span></button>
							</div>
							<div class="quantity">
								<button type="button" class="minus">-</button>
								<input type="number" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" data-cart="<?php echo $product['cart_id']; ?>" />
								<button type="button" class="plus">+</button>
							</div>
							<?php if ($product['price']) { ?>
							<div class="price">
								<span>150 руб.</span><?php echo $product['price']; ?>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
					<?php } else { ?>
					<p class="text-center"><?php echo $text_empty; ?></p>
					<?php } ?>
				</div>
				<?php foreach ($totals as $total) { if($total['code'] === 'sub_total') { ?>
				<div class="total">Сумма заказа: <span><?php echo $total['text']; ?></span></div>
				<?php }} ?>
				<div class="checkout">
					<a href="/checkout/" type="button" class="btn green">Оформить заказ</a>
				</div>
			</div>
		</div>
	</div>
</div>