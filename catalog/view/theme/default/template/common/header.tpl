<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>


    <link rel="stylesheet" media="all" href="catalog/view/theme/default/stylesheet/bootstrap.min.css"/>
    <link rel="stylesheet" media="all" href="catalog/view/theme/default/stylesheet/fonts.css"/>
    <link rel="stylesheet" media="all" href="catalog/view/theme/default/stylesheet/font-awesome/css/font-awesome.css"/>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
            integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
            crossorigin="anonymous"></script>

    <link href="catalog/view/theme/default/stylesheet/owl.carousel.min.css" type="text/css" rel="stylesheet"
          media="screen"/>
    <script src="catalog/view/javascript/owl.carousel.min.js"></script>

    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>

    <link rel="stylesheet" media="all" href="catalog/view/theme/default/stylesheet/styles.css"/>


    <script src="catalog/view/javascript/common.js"></script>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>


    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>"></script>
    <?php } ?>


    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>


</head>
<body class="<?php echo $body_class; ?>">


<div class="modal modal-info" id="addToCartModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-info-check"></div>
            <p>Товар успешно добавлен в корзину</p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn green" data-dismiss="modal">Продолжить покупки</a>
            <a href="/cart" class="btn green">Перейти в корзину</a>
        </div>
    </div>
</div>

<div class="modal modal-info" id="successModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-info-check"></div>
            <p></p>
        </div>
        <div class="modal-footer">
            <a href="#" class="btn green" data-dismiss="modal">Закрыть</a>
        </div>
    </div>
</div>


<div id="navigation">
    <div class="container pt-0 pb-0">

        <div class="logo">&nbsp;</div>
        <div class="links">&nbsp;</div>
        <div class="cart">&nbsp;</div>

    </div>
</div>


<header id="header">
    <div class="mobile-menu" id="mobileMenu">
        <div class="mobile-menu-top">
            <div class="container">
                <div class="row">
                    <div class="col-auto logo">
                        <?php if(!$is_home) { ?>
                        <a href="/" class="mobile-menu-logo">
                            <img src="catalog/view/theme/default/image/logo.png">
                        </a>
                        <?php } else { ?>
                        <img src="catalog/view/theme/default/image/logo.png">
                        <?php } ?>
                    </div>
                    <div class="col-auto ml-auto">
                        <a href="#" id="mobileMenuHide" class="btn-close mobile-menu-close"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-body">
            <div class="container">
                <div class="row">
                    <div id="headerCart768" class="col-auto"></div>
                    <div id="headerUser768" class="col-auto"></div>
                </div>
                <div class="links">
                    <div class="title">Разделы сайта</div>
                    <div id="headerMenu992">

                    </div>
                </div>
                <div class="links">
                    <div class="title">Каталог</div>
                    <div id="catalog992">

                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-bottom">
            <div class="container">
                <div id="headerPhone576"></div>
                <div class="mail"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
                <div class="socials" id="headerSocials768"></div>
            </div>
        </div>
    </div>
    <div class="top">
        <div class="container pt-0 pb-0">
            <div class="row">
                <div class="col-lg-auto location" id="headerLocation"><span>Доставка еды</span>
                    <div class="drop">
                        <button type="button" data-toggle="drop" data-target="#dropCity">Санкт-Петербург</button>
                        <div class="drop-box" id="dropCity">
                            <div id="dropCityContent">
                                <p>Ваш город <b>Санкт-Петербург?</b></p>
                                <div class="buttons">
                                    <a href="#" class="btn green" id="dropCityYes">Да</a>
                                    <a href="#" id="dropCityNo">Нет, я в другом городе</a>
                                </div>
                            </div>
                            <div id="dropCityNoContent" style="display: none">
                                <p>
                                    <mark>Извините!</mark>
                                    Мы пока осуществляем доставку только по Санкт-Петербургу и Ленинградской области
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($tagline) { ?>
                <div class="col-auto tagline ml-0 ml-lg-auto mr-auto"><span><?php echo $tagline; ?></span></div>
                <?php } ?>
                <div class="col-auto contacts">
                    <div id="headerPhone">
                        <div class="phone">
                            <a href="tel:<?php echo str_replace(array('(', ')', ' ', '-'), '', $telephone); ?>"><?php echo $telephone; ?></a>
                        </div>
                    </div>
                    <div class="socials" id="headerSocials">
                        <ul>
                            <?php if($vk) { ?>
                            <li class="vk"><a href="<?php echo $vk; ?>" target="_blank" class="fa fa-vk"><span>ВКонтакте</span></a></li>
                            <?php } ?>

                            <?php if($ok) { ?>
                            <li class="ok"><a href="<?php echo $ok; ?>" target="_blank" class="fa fa-odnoklassniki"><span>Одноклассники</span></a></li>
                            <?php } ?>

                            <?php if($fb) { ?>
                            <li class="facebook"><a href="<?php echo $fb; ?>" target="_blank" class="fa fa-facebook-f"><span>Facebook</span></a></li>
                            <?php } ?>

                            <?php if($insta) { ?>
                            <li class="instagram"><a href="<?php echo $insta; ?>" target="_blank" class="fa fa-instagram"><span>Instagram</span></a></li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>
                <a href="#" class="mobile-menu-btn" id="mobileMenuShow"></a>
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="container pt-0 pb-0">
            <div class="row justify-content-between justify-content-md-start justify-content-lg-start justify-content-xl-start">
                <div class="col-auto logo">
                    <?php if(!$is_home) { ?>
                    <a href="/">
                        <img src="catalog/view/theme/default/image/logo.png">
                    </a>
                    <?php } else { ?>
                    <img src="catalog/view/theme/default/image/logo.png">
                    <?php } ?>
                </div>
                <div class="col-auto mr-auto menu" id="headerMenu">
                    <nav>
                        <ul>
                            <li
                            <?php if ($_SERVER['REQUEST_URI'] == "/about/") { ?> class="active"<?php } ?>><a href="/about/">О нас</a></li>
                            <li
                            <?php if ($_SERVER['REQUEST_URI'] == "/contacts/") { ?> class="active"<?php } ?>><a href="/contacts/">Контакты</a></li>
                            <li
                            <?php if ($_SERVER['REQUEST_URI'] == "/delivery-payment/") { ?> class="active"<?php } ?>><a href="/delivery-payment/">Доставка и оплата</a></li>
                            <li
                            <?php if ($_SERVER['REQUEST_URI'] == "/blog/") { ?> class="active"<?php } ?>><a href="/blog/">Блог</a></li>
                        </ul>
                    </nav>
                </div>


                <div id="headerCart" class="col-auto"><?php echo $cart; ?></div>
                <div id="headerUser" class="col-auto">
                    <?php if($customer->isLogged()) { ?>
                        <div>Вы вошли как <br><a href="/account/"><?php echo $customer->getFirstname() . ' ' . $customer->getLastname(); ?></a></div>
                    <?php } else { ?>
                    <div class="user">
                        <a href="/sing-in/">Вход/регистрация</a>
                    </div>
                    <?php } ?>
                </div>
                <div id="headerLocation992"></div>
            </div>
        </div>
    </div>
</header>


<div id="catalog">
    <div class="container pt-0 pb-0">
        <ul>
            <?php foreach($categories as $category) { ?>
            <li>
                <a href="#category<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>

				