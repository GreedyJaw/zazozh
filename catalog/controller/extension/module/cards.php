<?php

class ControllerExtensionModuleCards extends Controller {
    public function index($setting) {
        static $module = 0;

        $data['heading_title'] = $setting['name'];

        $data['items'] = $setting['items'];

        $this->load->model('tool/image');

        foreach ($data['items'] as &$item) {
            if($item['image']) {
                $item['thumb'] = $this->model_tool_image->resize($item['image'], 270, 246);
            } else {
                $item['thumb'] = $this->model_tool_image->resize('no_image.png', 270, 246);
            }

        }

        $data['module'] = $module++;

        return $this->load->view('extension/module/cards', $data);
    }
}