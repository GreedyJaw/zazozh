<?php
// *	@copyright	OPENCART.PRO 2011 - 2017.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerExtensionModuleCatalog extends Controller {
    public function index() {
        $this->load->language('extension/module/catalog');

        $data['heading_title'] = $this->language->get('heading_title');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if(isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'pd.name';
        }

        if(isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories();

        foreach ($categories as $category) {
            $filter_data = array(
                'filter_category_id' => $category['category_id'],
                'sort' => $sort,
                'order' => $order
            );

            $products = array();

            $results = $this->model_catalog_product->getProducts($filter_data);

            if(!$results) continue;

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], 200, 200);
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', 200, 200);
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $special_percent = 100 - ($result['special'] * 100 / $result['price']);
                } else {
                    $special = false;
                    $special_percent = false;
                }

                if($special) {
                    $total = $this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax'));
                } else {
                    $total = $this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax'));
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }

                $results_img = $this->model_catalog_product->getProductImages($result['product_id']);

                $dop_img = array();

                foreach ($results_img as $result_img) {

                    if ($result_img['image']) {

                        $image_dop = $this->model_tool_image->resize($result_img['image'], 500, 500);

                    } else {

                        $image_dop = false;

                    }

                    $dop_img[0] = $image_dop;
                }

                $benefits = $this->model_catalog_product->getProductBenefitsbyProductId($result['product_id']);

                if($benefits) {
                    $benefit = array(
                        'image' => $this->model_tool_image->resize($benefits[0]['image'], 20, 20),
                        'name' => $benefits[0]['name']
                    );
                } else {
                    $benefit = false;
                }

                $products[] = array(
                    'product_id'  => $result['product_id'],
                    'dop_img'     => $dop_img,
                    'thumb'       => $image,
                    'name'        => $result['name'],
                    'description' => trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))),
                    'attribute_groups' => $this->model_catalog_product->getProductAttributes($result['product_id']),
                    'price'       => $price,
                    'special'     => $special,
                    'special_percent' => $special_percent,
                    'tax'         => $tax,
                    'rating'      => $rating,
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    'total' => $total,
                    'new' => $result['new'],
                    'hit' => $result['hit'],
                    'benefit' => $benefit,
                    'weight' => (int)$result['weight']
                );
            }

            $keys = array_column($products, 'product_id');

            $by_name = array_combine($keys, array_column($products, 'name'));
            $by_new = array_combine($keys, array_column($products, 'new'));
            $by_hit = array_combine($keys, array_column($products, 'hit'));
            $by_price = array_combine($keys, array_column($products, 'total'));

            asort($by_name);
            asort($by_new);
            asort($by_hit);
            asort($by_price);

            $sorts = array(
                'by_name' => array_keys($by_name),
                'by_new' => array_reverse(array_keys($by_new)),
                'by_hit' => array_reverse(array_keys($by_hit)),
                'by_price_up' => array_keys($by_price),
                'by_price_down' => array_reverse(array_keys($by_price))
            );

            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'name' => $category['name'],
                'products' => $products,
                'sorts' => $sorts
            );
        }

        return $this->load->view('extension/module/catalog', $data);
    }

    private function getStickers($product_id) {
        $stickers = $this->model_catalog_product->getProductStickerbyProductId($product_id) ;

        if (!$stickers) return;

        $data['stickers'] = array();

        foreach ($stickers as $sticker) {
            $data['stickers'][] = array(
                'position' => $sticker['position'],
                'image'    => HTTP_SERVER . 'image/' . $sticker['image']
            );
        }

        return $this->load->view('product/stickers', $data);
    }
}