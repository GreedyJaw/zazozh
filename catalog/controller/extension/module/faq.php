<?php

// *	@copyright	OPENCART.PRO 2011 - 2017.

// *	@forum	http://forum.opencart.pro

// *	@source		See SOURCE.txt for source and other copyright.

// *	@license	GNU General Public License version 3; see LICENSE.txt



class ControllerExtensionModuleFaq extends Controller {
    public function index($setting) {
        static $module = 0;

        $data['heading_title'] = $setting['name'];

        $data['items'] = $setting['items'];

        $data['module'] = $module++;

        return $this->load->view('extension/module/faq', $data);
    }
}