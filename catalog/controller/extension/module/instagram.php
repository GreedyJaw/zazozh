<?php

class ControllerExtensionModuleInstagram extends Controller {
    public function index($setting) {
        static $module = 0;

        $data['heading_title'] = $setting['name'];

        $data['link'] = $setting['link'];

        $data['items'] = $setting['items'];

        $this->load->model('tool/image');

        foreach ($data['items'] as &$item) {
            if($item['image']) {
                $item['thumb'] = HTTP_SERVER . 'image/' . $item['image'];
            } else {
                $item['thumb'] = $this->model_tool_image->resize('no_image.png', 300, 300);
            }

        }

        $data['module'] = $module++;

        return $this->load->view('extension/module/instagram', $data);
    }
}