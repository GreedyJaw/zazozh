<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-faq" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-faq" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                            <?php if ($error_name) { ?>
                            <div class="text-danger"><?php echo $error_name; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <div id="items" class="list-group">
                                <?php $i = 0; foreach($items as $item) { ?>
                                <div class="list-group-item row">
                                    <span class="handle"><i class="fa fa-arrows-alt"></i> </span>
                                    <div class="inputs row">
                                        <div class="col-sm-6">
                                            <label class="control-label">Заголовок</label>
                                            <input type="text" class="form-control" placeholder="Заголовок" value="<?php echo $item['title']; ?>" name="items[<?php echo $i; ?>][title]" />
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Текст</label>
                                            <textarea class="form-control" rows="5" placeholder="Текст" name="items[<?php echo $i; ?>][text]"><?php echo $item['text']; ?></textarea>
                                        </div>
                                    </div>
                                    <button type="button" class="text-danger remove removeItem"><i class="fa fa-times"></i></button>
                               </div>
                                <?php $i++; } ?>
                            </div>
                            <div class="pull-left">
                                <button type="button" class="btn btn-success" id="itemAdd">Добавить</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                            <select name="status" id="input-status" class="form-control">
                                <?php if ($status) { ?>
                                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                <option value="0"><?php echo $text_disabled; ?></option>
                                <?php } else { ?>
                                <option value="1"><?php echo $text_enabled; ?></option>
                                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    #items .list-group-item {
        position: relative;
        display: flex;
        align-items: center;
    }

    #items .row {
        margin-left: 0;
        margin-right: 0;
    }

    #items .inputs {
        flex: 1;
        padding-left: 20px;
        padding-right: 20px;
    }

    #items .list-group-item .handle {
        font-size: 18px;
        cursor: pointer;
    }

    #items .list-group-item label {
        padding-top: 0;
        margin-bottom: 5px;
    }

    #items .remove {
        padding: 0;
        background: transparent;
        border: 0;
        font-size: 20px;
    }
</style>

<script>
    var i = $('#items').children().length;

    $('#items').sortable();

    $('#itemAdd').click(function(){
       $('#items').append('<div class="list-group-item row">\n' +
           '                                    <span class="handle"><i class="fa fa-arrows-alt"></i> </span>\n' +
           '                                    <div class="inputs row">\n' +
           '                                        <div class="col-sm-6">\n' +
           '                                            <label class="control-label">Заголовок</label>\n' +
           '                                            <input type="text" class="form-control" placeholder="Заголовок" name="items[' + i + '][title]" />\n' +
           '                                        </div>\n' +
           '                                        <div class="col-sm-6">\n' +
           '                                            <label class="control-label">Текст</label>\n' +
           '                                            <textarea class="form-control" rows="5" placeholder="Текст" name="items[' + i + '][text]" ></textarea>\n' +
           '                                        </div>\n' +
           '                                    </div>\n' +
           '                                    <button type="button" class="text-danger remove removeItem"><i class="fa fa-times"></i></button>\n' +
           '                                </div>') ;

       i++;
    });

    $(document).on('click', '.removeItem', function(){
       $(this).closest('.list-group-item').remove();
    });
</script>

<?php echo $footer; ?>
