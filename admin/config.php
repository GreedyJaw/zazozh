<?php
// HTTP
define('HTTP_SERVER', 'http://zazozh.cf83387.tmweb.ru/admin/');
define('HTTP_CATALOG', 'http://zazozh.cf83387.tmweb.ru/');

// HTTPS
define('HTTPS_SERVER', 'https://zazozh.cf83387.tmweb.ru/admin/');
define('HTTPS_CATALOG', 'https://zazozh.cf83387.tmweb.ru/');

// DIR
define('DIR_APPLICATION', '/home/c/cf83387/zazozh/public_html/admin/');
define('DIR_SYSTEM', '/home/c/cf83387/zazozh/public_html/system/');
define('DIR_IMAGE', '/home/c/cf83387/zazozh/public_html/image/');
define('DIR_LANGUAGE', '/home/c/cf83387/zazozh/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/c/cf83387/zazozh/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/c/cf83387/zazozh/public_html/system/config/');
define('DIR_CACHE', '/home/c/cf83387/zazozh/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/c/cf83387/zazozh/public_html/system/storage/download/');
define('DIR_LOGS', '/home/c/cf83387/zazozh/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/c/cf83387/zazozh/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/c/cf83387/zazozh/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/c/cf83387/zazozh/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'cf83387_zazozh');
define('DB_PASSWORD', '7779777a');
define('DB_DATABASE', 'cf83387_zazozh');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
